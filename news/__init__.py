from .cli import cli
from .site import *
from .hacker_news import *
from .article import *

if __name__ == '__main__':
    cli()
