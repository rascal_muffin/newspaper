import newspaper
import tinydb


class Article(newspaper.Article):
    """An article with text, html, images..."""
    def __init__(self, if_parse, **kwargs):
        self.if_parse = if_parse
        self.url = kwargs.get('url', '')
        self.title = kwargs.get('title', '')
        self.text = kwargs.get('text', '')
        self.html = kwargs.get('html', '')
        self.log = kwargs.get('log', '')
        for k, v in kwargs.items():
            self.__setattr__(k, v)
        if self.if_parse:
            super(Article, self).__init__(self.url)
            self.log.debug('%s Downloading', self.url)
            self.download()
            self.log.debug('%s Parsing', self.url)
            self.parse()

    def save(self, db):
        """Save self to db"""
        props = {
            'title': self.title,
            'text': self.text,
            'html': self.html,
        }
        Q = tinydb.Query()
        # update if exists
        if db.search(Q.url == self.url):
            db.update(props, Q.url == self.url)
        # insert otherwise
        props['url'] = self.url
        db.insert(props)
