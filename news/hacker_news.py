from .site import Site
from .article import Article
import requests
from bs4 import BeautifulSoup
from newspaper import ArticleException


class HackerNews(Site):
    """https://news.ycombinator.com/"""
    API_TOP = 'https://hacker-news.firebaseio.com/v0/topstories.json'
    API_ITEM = 'https://hacker-news.firebaseio.com/v0/item/{item_id}.json'
    HTML_TOP = 'https://news.ycombinator.com/'

    def crawl(self, limit=100):
        self.log.debug('Getting items')
        text = requests.get(self.HTML_TOP).text
        soup = BeautifulSoup(text, 'lxml')
        links = soup.find_all('a', class_='storylink')
        items = [i.attrs['href'] for i in links][:limit]
        self.log.debug('Got items %s', str(items))
        for url in items:
            self.log.debug('Got item %s', url)
            try:
                self.articles[url] = Article(True, url=url, log=self.log)
            except ArticleException as e:
                self.log.error('Error creating article, %s', str(e))

    def save(self, db):
        """Save all articles"""
        for url, article in self.articles.items():
            self.log.debug('Saving %s' % url)
            article.save(db)

    def load(self, db):
        """Load all articles"""
        for i in db:
            self.articles[i['url']] = Article(False, **i)
        self.log.info([i.url for i in self.articles])
