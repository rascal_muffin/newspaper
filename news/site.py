from .article import Article
import tinydb


class Site(object):
    """A directory of articles"""
    API = 'http://example.com'

    def __init__(self, log=None):
        self.log = log
        self.articles = dict()
        self.archive_dir = './news_archive'

    def crawl(self):
        """Get articles"""
        raise NotImplementedError
