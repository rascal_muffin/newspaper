import news
import tinydb


def get_logger(name,
               level='WARNING',
               log_file_prefix=None,
               max_bytes=100000,
               backup_count=10):
    """Make a logger that write to both stdout and file"""
    import logging.handlers
    import os

    # log level
    num_level = getattr(logging, level.upper(), None)

    # create formatter and add it to the handlers
    formatter = logging.Formatter(
        '%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    # create logger with name
    logger = logging.getLogger(name)
    logger.setLevel(num_level)

    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(num_level)
    ch.setFormatter(formatter)
    logger.addHandler(ch)

    # create file handler which logs even debug messages
    if log_file_prefix:
        fh = logging.handlers.RotatingFileHandler(
            os.path.join(log_file_prefix, '{}.log'.format(name)),
            maxBytes=max_bytes,
            backupCount=backup_count)
        fh.setLevel(num_level)
        fh.setFormatter(formatter)
        logger.addHandler(fh)

    return logger


def test_cli():
    print('Hello test')


def test_article():
    hn = news.HackerNews(get_logger('test_news', level='DEBUG'))
    hn.crawl(limit=5)
    db = tinydb.TinyDB('test_sites.json')
    hn.save(db)


def main():
    print('Hello test')


if __name__ == '__main__':
    main()
